import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreated = false;
  serverCreationStatus = 'No server was created.';
  serverName = 'Two-Way Binding';
  servers = ['TestServer 1', 'TestServer 2']

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
      console.log('allowNewServer = TRUE');
    }, 2000);
  }

  ngOnInit() {
  }

  onCreateServer() {
    this.serverCreationStatus = 'Server was created!';
    this.serverCreated = true;
    this.servers.push(this.serverName);
  }

  onUpdateServerName(e: any) {
    console.log(e);
    this.serverName = (<HTMLInputElement>event.target).value;
  }

}
