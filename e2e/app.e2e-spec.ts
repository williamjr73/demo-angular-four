import { DemoAngularFourPage } from './app.po';

describe('demo-angular-four App', () => {
  let page: DemoAngularFourPage;

  beforeEach(() => {
    page = new DemoAngularFourPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
